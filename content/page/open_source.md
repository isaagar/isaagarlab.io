---
title: "Current Work"
linkTitle: Open Source
date: 2018-01-24T03:20:44+05:30
menu: main
weight: -240 
---

Currently working on following projects

* Localization of [Calamares Installer](https://calamares.io)(Hindi,Marathi)
* Packaging [Loomio Installer](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=894722)
