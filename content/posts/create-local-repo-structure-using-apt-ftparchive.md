---
title: "Create Local Repo Structure Using apt-ftparchive"
date: 2018-01-24T01:22:33+05:30
tags:
  - '#apt'
---

**apt-ftparchive** is used for generating index files. I’m mentioning following steps to create index files out of existing **pool** directory.

Go to the directory in which **pool** and **dists** directory are placed. If you want to create dists from scratch again then you should delete the dists.

Lets create directories for **dists** structure
```bash
$ mkdir -p dists/<codename>/main/binary-<arch>
```

#### Create packages file
```bash
$ apt-ftparchive packages pool/dists/main/<codename>/binary-<arch>/Packages
$ gzip -9 -c Packages > Packages.gz
```

#### Create Release file
```bash
$ apt-ftparchive release dists/main/< codename >/binary-$arch > dists/main/$codename/binary-$arch/Release
```

The above generated **Release** file contains only checksums.

To add codename,version etc . Create **`apt.conf`** like below :

```bash
APT {
FTPArchive {
Release {
Origin “Hamara Sugam”;
Label “Hamara Sugam”;
Suite “namaste”;
Version “2.0”;
Codename “namaste”;
Architectures “amd64”;
Components “main contrib non-free”;
};
};
};
```

Now, again generate **Release** file using `**apt.conf**`

```bash
$ apt-ftparchive -c apt.conf release dists/namaste > dists/namaste/Release
```

Specifying options on command line instead of creating config file

```bash
$ apt-ftparchive -o APT::FTPArchive::Release::Origin=”Hamara Sugam” \
-o APT::FTPArchive::Release::Label=”Hamara Sugam” \
-o APT::FTPArchive::Release::Suite=”namaste” \
-o APT::FTPArchive::Release::Version=”2.0″ \
-o APT::FTPArchive::Release::Codename=”namaste” \
-o APT::FTPArchive::Release::Architectures=”amd64″ \
-o APT::FTPArchive::Release::Components=”main contrib non-free” \
release dists/namaste/ > dists/namaste/Release
```
