---
title: "Convert Pdf to Image"
date: 2018-06-14T11:04:14+05:30
tags:
  - '#imagemagick'
---

Converting pdf files to image using `convert` command line tool provided by imagemagick.

On `apt` systems , install `imagemagick` using following command
```bash
# apt install imagemagick
```

Converting pdf file to image,
```bash
convert -background white -alpha remove -density 250 -quality 90  input.pdf output.png
```
Above command generates image file for each page in pdf file like `output-0.png , output-1.png,...`


For converting first page of pdf to image,
```bash
convert -background white -alpha remove -density 250 -quality 90 inut.pdf[0] firstpage.png
```
#### Appending images

Appending image files into single image file in top-bottom,
```bash
convert -append out-0.png out-1.jpg page_0-1.png
```

Appending image files into single image file in left to right sequence ,
```bash
convert +append out-0.png out-1.jpg page_0-1.png
```
