---
title: "Setting Up Ubuntu Server Using virt-install"
date: 2018-09-13T11:25:46+05:30
draft: false
---

Install packages
```bash
$ sudo apt install libvirt-daemon qemu-kvm  libvirt-bin
```

Using virt-install
```bash
$ sudo virt-install --name ubuntu --ram 1024 --disk path=/var/lib/images/ubuntu_server,size=20 \
 --vcpus 1 --os-type linux --os-variant generic \
 --nographics --extra-args "console=ttyS0 " --location http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/ \
 --extra-args "auto url=http://192.168.32.152:8000/ubuntu.seed" 
```
For debugging purpose, you can add `--debug` in above command.
 
